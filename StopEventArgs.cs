﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDelegateEvents
{
    class StopEventArgs : EventArgs
    {
        public string Message { get; } = "Search was stoped";
    }
}

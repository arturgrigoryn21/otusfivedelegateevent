﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDelegateEvents
{
    class User : IComparable
    {
        public User(int id)
        {
            ID = id;
        }
        public int ID { get; set; }

        public int CompareTo(object obj)
        {
            User user = (User)obj;
            if (user != null)
                return this.ID.CompareTo(user.ID);
            else
                throw new Exception("Невозможно сравнить два объекта");
        }
    }
}

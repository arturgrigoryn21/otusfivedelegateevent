﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq.Expressions;
using System.Linq;
using System.Timers;

namespace OtusDelegateEvents
{
    class Task2
    {
        public event EventHandler<FindEventArgs> findEvent;
        public event EventHandler<StopEventArgs> stopEvent;
        static string path = "file";

        string[] fileCatalog
        {
            get
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        return Directory.GetFiles(path);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return null;
                    }
                }
                else
                {
                    Console.WriteLine("Directory wasn't found");
                    return null;
                }
            }
        }
        public Task2()
        {
            stopEvent += StopEvent_Handler;
            findEvent += FindEvent_Handler;
        }
        public void StopEvent_Handler(object s, StopEventArgs ev)
        {
            Console.WriteLine(ev.Message);
        }
        public void FindEvent_Handler(object s, FindEventArgs ev)
        {
            Console.WriteLine(ev.Message);
        }
        public void FileSeeker(params string[] files)
        {
            foreach(var file in files)
            {
                Console.WriteLine("Input \"exit\" to stop the search");
                if (Console.ReadLine() == "exit")
                {
                    stopEvent?.Invoke(this, new StopEventArgs());
                    break;
                }
                if (fileCatalog.Contains(file))
                {                   
                    findEvent?.Invoke(this, new FindEventArgs());                   
                }
                
            }
        }
    }    
}

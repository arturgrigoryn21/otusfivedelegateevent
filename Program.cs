﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Timers;

namespace OtusDelegateEvents
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task2 task2 = new Task2();
            //task2.FileSeeker("rrr", "file\\ftdfdf.docx");

            IEnumerable<User> ts1 = new List<User>()
            {
                new User(2),
                new User(6),
                new User(1)
            };
            var result = ts1.GetMax(x => x.ID);
            Console.WriteLine(result);
        }
    }    
}

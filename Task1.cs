﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace OtusDelegateEvents
{
    public static class Task1
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            //Я знаю что это не работает. Просто сделал что бы компиляция проходила
            var result = getParametr?.Invoke(e.Max<T>());
            return result as T;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusDelegateEvents
{
    class FindEventArgs : EventArgs
    {
        public string Message { get; } = "File was found";
    }
}
